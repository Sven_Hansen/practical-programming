#include<stdio.h>
#include<limits.h>
#include<float.h>

int main () {

	float floatEps = FLT_EPSILON;
	double doubleEps = DBL_EPSILON;
	long double longEps = LDBL_EPSILON;

	/* float epsilon with while loop */
	float f = 1;
	while(f+1 != 1) {
		f/=2;
	}
	f*=2;
	printf("float epsilon: %g\n", floatEps);
	printf("float epsilon found with while loop: %g\n", f);

	/* double epsilon with for loop */
	double d = 1;
	for(d = 1; d+1 != 1; d/=2) {
	}
	d*=2;
	printf("double epsilon: %g\n", doubleEps);
	printf("double epsilon found with for loop: %g\n", d);

	/* long double epsilon with do while loop */
	long double l = 1;
	do {
		l/=2;
	} while(l+1 != 1);
	l*=2;
	printf("long double epsilon: %Lg\n", longEps);
	printf("long double epsilon found with do while loop: %Lg\n \n", l);


	return 0;
}
