max integer: 2147483647
max integer with while loop: 2147483647
max integer with for loop: 2147483647
max integer with do while loop: 2147483647
 
min integer: -2147483648
min integer with while loop: -2147483648
min integer with for loop: -2147483648
min integer with do while loop: -2147483648
 
float epsilon: 1.19209e-07
float epsilon found with while loop: 1.19209e-07
double epsilon: 2.22045e-16
double epsilon found with for loop: 2.22045e-16
long double epsilon: 1.0842e-19
long double epsilon found with do while loop: 1.0842e-19
 
Summing up and down with floats:
sum_up_float: 15.4037
sum_down_float: 18.8079
Summing up and down with doubles:
sum_up_double: 20.9662
sum_down_double: 20.9662

When a very small number, a, is added to a comparatively large number, b, it is simply reduced to a + b = b. Therefore it stops adding the small numbers when we sum up.
Since double can handle more decimels we do not reach that point and get matching results for summing up and down with double.

a = 5 and b = 10 tau = 1 epsilon = 2
a and b are equal within tau or epsilon
a = 5 and b = 1000 tau = 1 epsilon = 1
a and b are not equal within tau or epsilon
