#include<stdio.h>
#include"equal.h"

int main () {
	double a = 5;
	double b = 10;
	double c = 1;
	double d = 2;
	int result = equal(a, b, c, d);
	printf("a = %g and b = %g tau = %g epsilon = %g\n", a, b, c, d);
	if (result == 0) {
		printf("a and b are not equal within tau or epsilon\n");
	} else {
		printf("a and b are equal within tau or epsilon\n");
	}
	
	double a2 = 5;
	double b2 = 1000;
	double c2 = 1;
	double d2 = 1;
	int result2 = equal(a2, b2, c2, d2);
	printf("a = %g and b = %g tau = %g epsilon = %g\n", a2, b2, c2, d2);
	if (result2 == 0) {
		printf("a and b are not equal within tau or epsilon\n");
	} else {
		printf("a and b are equal within tau or epsilon\n");
	}

	return 0;
}
