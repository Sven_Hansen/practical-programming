#include<stdio.h>
#include<stdlib.h>
#include"nvector.h"

nvector* nvector_alloc (int n) {
	nvector* v = (nvector *) malloc(sizeof(nvector));
	(*v).size = n;
	(*v).data = (double *) malloc(n * sizeof(nvector));
	if(v == NULL) {
		fprintf(stderr, "error in nvector_alloc\n");
	}
	return v;
}

void nvector_free (nvector* v) {
	free(v->data);
	free(v);
	v = NULL;
}

void nvector_set (nvector* v, int i, double value) {
	v->data[i] = value;
}

double nvector_get (nvector* v, int i) {
	return v->data[i];
}

double nvector_dot_product (nvector* u, nvector* v) {
	double result = 0;
	printf("size %d\n", v->size);
	for (int i=0; i<u->size; i++) {
		result += u->data[i] * v->data[i];
	}
	return result;
}
