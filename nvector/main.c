#include<stdio.h>
#include<stdlib.h>
#include"nvector.h"

int main () {

	int memory = 5;
	nvector* nv = nvector_alloc (memory);
	printf("nv after malloc: %p\n", nv); 
	printf("size of nv after malloc: %d\n\n", nv->size);
	
	double value1 = 5.0, value2 = 1.5, value3 = 15.5;
	nvector_set(nv, 1, value1);
	nvector_set(nv, 2, value2);
	nvector_set(nv, 3, value3);
	double nvdata1 = nvector_get(nv, 1);
	double nvdata2 = nvector_get(nv, 2);
	double nvdata3 = nvector_get(nv, 3);
	printf("data in nv from nvector_get\nnvdata1: %g\nnvdata2: %g\nnvdata3: %g\n\n", nvdata1, nvdata2, nvdata3);
	
	double dotProduct = nvector_dot_product (nv, nv);
	printf("dot product nv²: %g\n\n", dotProduct);

	nvector_free (nv);
	printf("nv afer free: %p\n", nv);
	printf("size of nv after free: %d\n", nv->size);

	return 0;
}
