#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int diff_equation (double t, const double y[], double dydt[], void * params) {
	dydt[0] = y[0]/(2*t);

	return GSL_SUCCESS;
}

double sqrt_function (double x) {
	assert (x>=0);
	if (x==0) return 0;
	if (x<1) return 1/sqrt_function(1/x);
	if (x>16) return 4*sqrt_function(x/16);
	if (x>4) return 2*sqrt_function(x/4);

	gsl_odeiv2_system sys;
	sys.function = diff_equation;
	sys.jacobian = NULL;
	sys.dimension = 1;
	sys.params = NULL;

	double acc=1e-6, eps=1e-6, hstart=copysign (0.1,x);
	gsl_odeiv2_driver *driver = gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rkf45, hstart, acc, eps);

	double t=1, y[1]={1};
	gsl_odeiv2_driver_apply (driver, &t, x, y);

	gsl_odeiv2_driver_free (driver);
	return y[0];
}

int main () {
	printf ("x\t ODE\t\t math.h\n");
	for(double x=0; x<=20.1; x+=0.1) {
		printf ("%g\t %g\t %g\t\n", x, sqrt_function(x), sqrt(x));
	}

	return 0;
}


