#include <stdio.h>
#include <math.h>
#include <complex.h>

int main(void) {
	double gam = tgamma(5);
	printf("Gamma function of 5 is %g\n", gam);

	double bes = j0(5);
	printf("Bessel function of the first kind of 0.5 is %g\n", bes);


	double complex chalf = 0.5;
	double complex cnum = -2.0;
	double complex croot = cpow(cnum, chalf);
	double root = cimag(croot);
	printf("Complex squareroot of -2 is %g\n", root);

	double complex ci = I;
	double complex cfunc = cexp(ci);
	double cpart = cimag(cfunc);
	double rpart = creal(cfunc);
	printf("Imaginary part of e^i is %g and the real part is %g\n", cpart, rpart);

	double complex cpi = cexp(ci*M_PI);
	double cpartpi = cimag(cpi);
	double rpartpi = creal(cpi);
	printf("Imaginary part of e^(pi*i) is %g and the real part is %g\n", cpartpi, rpartpi);

	double complex e = M_E;
	double complex ie = cpow(ci, e);
	double cparte = cimag(ie);
	double rparte = creal(ie);
	printf("Imaginary part of i^e is %g and the real part is %g\n", cparte, rparte);

	float f = 0.1111111111111111111111111111;
	double d = 0.1111111111111111111111111111;
	long double l = 0.1111111111111111111111111111L;
	printf("0.1111111111111111111111111111 is stored as\n%.25g for float\n%.25lg for double\n%.25Lg for long double", f, d, l);

	return 0;	
}
