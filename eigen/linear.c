#include<stdio.h>
#include<math.h>
#include<gsl/gsl_linalg.h>
#include<gsl/gsl_eigen.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_eigen.h>


int main () {
	const int n = 3;

	gsl_matrix *A = gsl_matrix_calloc (n, n);
	gsl_vector *b = gsl_vector_calloc (n);
	gsl_vector *x = gsl_vector_calloc (n);
	gsl_vector *real = gsl_vector_calloc (n);

	//Theoretical
	gsl_vector_set (real, 0, -1.1379);
	gsl_vector_set (real, 1, -2.8330);
	gsl_vector_set (real, 2, 0.8515);

	//System of linear equations
	gsl_matrix_set (A, 0, 0, 6.13);
	gsl_matrix_set (A, 0, 1, -2.90);
	gsl_matrix_set (A, 0, 2, 5.86);

	gsl_matrix_set (A, 1, 0, 8.08);
	gsl_matrix_set (A, 1, 1, -6.31);
	gsl_matrix_set (A, 1, 2, -3.89);

	gsl_matrix_set (A, 2, 0, -4.36);
	gsl_matrix_set (A, 2, 1, 1.0);
	gsl_matrix_set (A, 2, 2, 0.19);

	gsl_vector_set (b, 0, 6.23);
	gsl_vector_set (b, 1, 5.37);
	gsl_vector_set (b, 2, 2.29);

	//Solve the system of equations
	gsl_linalg_HH_solve (A, b, x);

	fprintf (stdout, "Solution found with Householder:\n");
	gsl_vector_fprintf (stdout, x, "%g\n");

	fprintf (stdout, "Theoretical solution:\n");
	gsl_vector_fprintf (stdout, real, "%g\n");

	//Free allocated memory
	gsl_vector_free (b);
	gsl_vector_free (x);
	gsl_vector_free (real);
	gsl_matrix_free (A);

	return 0;	
}
