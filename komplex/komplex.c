#include<stdio.h>
#include<float.h>
#include<math.h>
#include<complex.h>
#include"komplex.h"


void komplex_print (char* s, komplex z) {
	printf("String s: %s\nKomplex z: %g + %gi\n", s, (z).re, (z).im);
}

void komplex_set (komplex* z, double x, double y) {
	(*z).re = x;
	(*z).im = y;
}


komplex komplex_new (double x, double y) {
	komplex d;
	d.re = x;
	d.im = y;
	return d;
}

komplex komplex_add (komplex a, komplex b) {
	komplex z = { a.re + b.re, a.im + b.im };
	return z;
}

komplex komplex_sub (komplex a, komplex b) {
	komplex z = { a.re - b.re, a.im - b.im };
	return z;
}






