#include"komplex.h"
#include<stdio.h>
#include<math.h>
#include<float.h>
#include<complex.h>

int main () {

	char* letter = "A";
	komplex a = {1,2}, b = {3,4};
	komplex c = {5,6}, d, e, f;
	double x = 1.0, y = 2.0;

	printf("komplex_print with input A and 1,2:\n");
	komplex_print(letter, a);

	printf("\nkomplex c before komplex_set: 5 + 6i\n");
	komplex_set (&c, x, y);
	printf("komplex c after komplex_set with input (c, 1.0, 2.0): %g + %gi\n", (c).re, (c).im);
	
	d = komplex_new(x, y);
	printf("\nkomplex d made from double x=1.0 and y=2.0: %g + %gi\n", d.re, d.im);

	e = komplex_add(a, b);
	printf("\nkomplex e made from komplex a=1,2 and b=3,4: %g + %gi\n", e.re, e.im);

	f = komplex_sub(a, b);
	printf("\nkomplex f made from komplex a=1,2 minus b=3,4: %g + %gi\n", f.re, f.im);

	return 0;
}
